package es.ua.expertojava.todo

class TodoTagLib {
    static defaultEncodeAs = [taglib:'none']
    //static encodeAsForTags = [tagName: [taglib:'html'], otherTagName: [taglib:'none']]

    static namespace = 'todo'

    def printIconFromBoolean = {attrs ->
        if (attrs.value)
            out << asset.image(src:"complete.png", width: "25", height: "25")
        else
            out << asset.image(src:"incomplete.png", width: "25", height: "25")
    }
}
