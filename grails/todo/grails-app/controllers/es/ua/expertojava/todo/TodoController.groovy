package es.ua.expertojava.todo



import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
class TodoController {

    def todoService

    def springSecurityService

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond Todo.findAllByUser(springSecurityService.currentUser, params), model:[todoInstanceCount: Todo.count()]
    }

    def indexSearch() {
        render(view:'index', model: [todoInstanceList: Todo.search([result: 'every']) {
            must(queryString(params.q))
            must(term('$/Todo/user/id',User.findByUsername(springSecurityService.currentUser)?.id))
        }])
    }

    def showTodosByUser(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        println(User.findByUsername(params.username))
        respond Todo.findAllByUser(User.findByUsername(params.username), params), model:[todoInstanceCount: Todo.count()]
    }

    def listCategories() {
        respond Category.list(), model:[categoryInstanceCount: Category.count()]
    }

    def listByCategory(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        params.sort = "date"
        params.order = "Desc"
        List<Category> categoryList = new ArrayList<Category>()
        if([Collection, Object[]].any { it.isAssignableFrom(params.categories.getClass())}) {
            params.categories.each { category ->
                Category c = Category.findByName(category)
                categoryList.add(c)
            }
        } else {
            Category c = Category.findByName(params.categories)
            categoryList.add(c)
        }
        respond Todo.findAllByCategoryInListAndUser(categoryList, springSecurityService.currentUser, params), model: [todoInstanceCount: Todo.count()]
    }

    def show(Todo todoInstance) {
        respond todoInstance
    }

    def create() {
        respond new Todo(params)
    }

    @Transactional
    def save(Todo todoInstance) {
        if (todoInstance == null) {
            notFound()
            return
        }

        if (todoInstance.hasErrors()) {
            respond todoInstance.errors, view:'create'
            return
        }

        todoService.saveTodo(todoInstance)

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'todo.label', default: 'Todo'), todoInstance.id])
                redirect todoInstance
            }
            '*' { respond todoInstance, [status: CREATED] }
        }
    }

    def edit(Todo todoInstance) {
        respond todoInstance
    }

    @Transactional
    def update(Todo todoInstance) {
        if (todoInstance == null) {
            notFound()
            return
        }

        if (todoInstance.hasErrors()) {
            respond todoInstance.errors, view:'create'
            return
        }

        todoService.saveTodo(todoInstance)

        todoInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'Todo.label', default: 'Todo'), todoInstance.id])
                redirect todoInstance
            }
            '*'{ respond todoInstance, [status: OK] }
        }
    }

    @Transactional
    def delete(Todo todoInstance) {

        if (todoInstance == null) {
            notFound()
            return
        }

        todoService.delete(todoInstance)

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'Todo.label', default: 'Todo'), todoInstance.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'todo.label', default: 'Todo'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
