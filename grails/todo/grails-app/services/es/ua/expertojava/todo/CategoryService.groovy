package es.ua.expertojava.todo

import grails.transaction.Transactional

@Transactional
class CategoryService {

    @Transactional
    def delete(Category categoryInstance) {

        categoryInstance.getTodos().each { todo ->
            todo.setCategory(null)
        }

        categoryInstance.delete flush:true
    }
}
