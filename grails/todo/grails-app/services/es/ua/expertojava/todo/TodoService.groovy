package es.ua.expertojava.todo

import grails.transaction.Transactional
import groovy.time.TimeCategory

@Transactional
class TodoService {

    @Transactional
    def delete(Todo todoInstance) {

        def tags = todoInstance.tags.findAll()
        tags?.each { tag ->
            todoInstance.removeFromTags(tag)
        }

        todoInstance.delete flush:true

    }

    def springSecurityService

    @Transactional
    def saveTodo(Todo todoInstance) {

        if(todoInstance.complete){
            todoInstance.setDateDone(todoInstance.getDateCreated())
        }
        todoInstance.setUser(springSecurityService.currentUser)

        todoInstance.save flush:true
    }

    @Transactional
    def lastTodosDone(Integer hours) {
        Date now = new Date(System.currentTimeMillis())
        Date before = new Date(System.currentTimeMillis())
        use (TimeCategory) {
            before -= hours.hours
        }

        return Todo.findAllByDateDoneBetween(before, now)
    }
}
