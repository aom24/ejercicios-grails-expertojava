package todo

class LogFilters {

    def springSecurityService

    def filters = {
        all(controller:'todo|category|tag', action:'create|edit|index|show') {
            before = {

            }
            after = { Map model ->
                log.trace("User ${springSecurityService.currentUser} - Controlador ${controllerName} - Accion ${actionName} - Modelo ${model}")
            }
            afterView = { Exception e ->

            }
        }
    }
}
