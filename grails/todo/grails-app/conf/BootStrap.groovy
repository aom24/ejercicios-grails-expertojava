import es.ua.expertojava.todo.*

class BootStrap {

    def init = { servletContext ->
       try {
           def categoryHome = new Category(name: "Hogar").save()
           def categoryJob = new Category(name: "Trabajo").save()

           def tagEasy = new Tag(name: "Fácil").save()
           def tagDifficult = new Tag(name: "Difícil").save()
           def tagArt = new Tag(name: "Arte").save()
           def tagRoutine = new Tag(name: "Rutina").save()
           def tagKitchen = new Tag(name: "Cocina").save()

           def todoPaintKitchen = new Todo(title: "Pintar cocina", date: new Date() + 1, complete: false)
           def todoCollectPost = new Todo(title: "Recoger correo postal", date: new Date() + 2, complete: true)
           def todoBakeCake = new Todo(title: "Cocinar pastel", date: new Date() + 4, complete: false)
           def todoWriteUnitTests = new Todo(title: "Escribir tests unitarios", date: new Date(), complete: false)

           todoPaintKitchen.addToTags(tagDifficult)
           todoPaintKitchen.addToTags(tagArt)
           todoPaintKitchen.addToTags(tagKitchen)
           todoPaintKitchen.category = categoryHome
           todoPaintKitchen.save()

           todoCollectPost.addToTags(tagRoutine)
           todoCollectPost.category = categoryJob
           todoCollectPost.save()

           todoBakeCake.addToTags(tagEasy)
           todoBakeCake.addToTags(tagKitchen)
           todoBakeCake.category = categoryHome
           todoBakeCake.save()

           todoWriteUnitTests.addToTags(tagEasy)
           todoWriteUnitTests.category = categoryJob
           todoWriteUnitTests.save()

           def roleAdmin = new Role(authority: "ROLE_ADMIN").save()
           def roleBasic = new Role(authority: "ROLE_BASIC").save()

           def admin = new User(username: "admin", password: "admin", name: "admin_name", surnames: "admin_surnames", email: "admin_email@mail.com", confirmPassword: "admin").save()
           def usuario1 = new User(username: "usuario1", password: "usuario1", name: "usuario1_name", surnames: "usuario1_surnames", email: "usuario1_email@mail.com", confirmPassword: "usuario1").save()
           def usuario2 = new User(username: "usuario2", password: "usuario2", name: "usuario2_name", surnames: "usuario2_surnames", email: "usuario2_email@mail.com", confirmPassword: "usuario2").save()

           PersonRole.create admin, roleAdmin, true
           PersonRole.create usuario1, roleBasic, true
           PersonRole.create usuario2, roleBasic, true

           todoPaintKitchen.user = usuario1
           todoPaintKitchen.save()
           todoBakeCake.user = usuario2
           todoBakeCake.save()
           todoCollectPost.user = usuario1
           todoCollectPost.save()
           todoWriteUnitTests.user = usuario2
           todoWriteUnitTests.save()
       } catch (Exception e) {
            e.printStackTrace()
        }
    }
    def destroy = { }
}
