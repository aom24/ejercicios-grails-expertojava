<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="main">
    <g:set var="entityName" value="${message(code: 'todo.label', default: 'Todo')}" />
    <title>Seleccion de categorías</title>
</head>

<body>
<a href="#create-todo" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
<div class="nav" role="navigation">
    <ul>
        <li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
        <li><g:link class="list" action="index"><g:message code="default.list.label" args="[entityName]" /></g:link></li>
    </ul>
</div>
<div id="list-category" class="content scaffold-list" role="main">
    <h1>Seleccione categoría</h1>
    <g:if test="${flash.message}">
        <div class="message" role="status">${flash.message}</div>
    </g:if>
    <div style="margin-left: 30px">
        <form action="listByCategory" method="get">
            <g:each in="${categoryInstanceList}" status="i" var="categoryInstance">
                <input type="checkbox" name="categories" value=${fieldValue(bean: categoryInstance, field: "name")}><b> ${fieldValue(bean: categoryInstance, field: "name")}</b>
                </br>
            </g:each>
            </br>
            <input type="submit" value="Filtrar">
        </form>
    </div>
</div>
</body>
</html>