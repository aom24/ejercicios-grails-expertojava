package es.ua.expertojava.todo

class Todo {
    String title
    Boolean complete
    Date date
    Date reminderDate
    Date dateDone
    String url
    String description
    Category category

    Date dateCreated

    User user

    static hasMany = [tags:Tag]
    static belongsTo = [Tag]

    static constraints = {
        title(blank:false)
        complete(nullable:false)
        date(nullable:false)
        reminderDate(nullable:true,
                validator: { val, obj ->
                    if (val && obj.date) {
                        return val.before(obj?.date)
                    }
                    return true
                }
        )
        dateDone(nullable:true)
        url(nullable:true, url:true)
        description(blank:true, nullable:true, maxSize:1000)
        category(nullable:true)
        user(nullable: true)
    }

    static searchable = true

    String toString(){
        title
    }
}
