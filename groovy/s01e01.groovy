class Todo {

    String titulo
    String descripcion

    Todo(String tit, String des) {
        this.titulo = tit
        this.descripcion = des
    }
}

def todos = [new Todo("Lavadora","Poner lavadora"),new Todo("Impresora","Comprar cartuchos impresora"),new Todo("Películas","Devolver películas videoclub")]
todos.each{todo -> println("${todo.getTitulo()} ${todo.getDescripcion()}")}