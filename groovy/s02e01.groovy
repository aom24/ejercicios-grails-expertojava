def factorial = { num -> 
    def total = 1
    1.upto(num) { numero ->
       total *= numero
    }
    return total
}

assert factorial(1)==1
assert factorial(2)==1*2
assert factorial(3)==1*2*3
assert factorial(4)==1*2*3*4
assert factorial(5)==1*2*3*4*5
assert factorial(6)==1*2*3*4*5*6
assert factorial(7)==1*2*3*4*5*6*7
assert factorial(8)==1*2*3*4*5*6*7*8
assert factorial(9)==1*2*3*4*5*6*7*8*9
assert factorial(10)==1*2*3*4*5*6*7*8*9*10

def lista = [1,2,3,4,5,6,7,8,9,10].collect { num -> factorial(num) }
println lista

def ayer = { fecha ->
    def ayer = new Date().parse('d/M/yyyy H:m:s', fecha) - 1
    return ayer.format('d/M/yyyy H:m:s')
}

def manyana = { fecha ->
    def manyana = new Date().parse('d/M/yyyy H:m:s', fecha) + 1
    return manyana.format('d/M/yyyy H:m:s')
}

def fechas = ["28/6/2008 00:30:20", "15/8/2010 15:20:50", "31/7/2015 00:30:20", "1/3/2006 00:30:20"]
def fechasAyer = fechas.collect { ayer(it) }
def fechasManyana = fechas.collect { manyana(it) }

println fechasAyer
println fechasManyana