class Libro {
    String nombre
    Integer anyo
    String autor
    String editorial
    
    Libro(String nombre, Integer anyo, String autor) {
        this.nombre = nombre
        this.anyo = anyo
        this.autor = autor
    }
    
    String getAutor() {
        def (apellidos, nombre) = this.autor.tokenize( "," )
        return "${nombre.trim()} $apellidos"
    }
}

def l1 = new Libro("La colmena", 1951,  "Cela Trulock, Camilo José")
def l2 = new Libro("La galatea", 1585, "Cervantes Saavedra, Miguel")
def l3 = new Libro("La dorotea", 1632, "Lope de Vega y Carpio, Félix Arturo")

assert l1.getNombre() == 'La colmena'
assert l2.getAnyo() == 1585
assert l3.getAutor() == 'Félix Arturo Lope de Vega y Carpio'

l1.setEditorial("Anaya")
l2.setEditorial("Planeta")
l3.setEditorial("Santillana")

assert l1.getEditorial() == 'Anaya'
assert l2.getEditorial() == 'Planeta'
assert l3.getEditorial() == 'Santillana'