class Calculadora {
   
    static void main(String... args) {          
    def operacion =   System.in.withReader { it ->
        def lista = new ArrayList()
            print 'Introduzca operador uno: '
            lista.add(it.readLine())
            print 'Introduzca operador dos: '
            lista.add(it.readLine())
            print 'Introduzca operando (+,-,*,/): '
            lista.add(it.readLine())
            return lista
        }    

       def resultado = calcula(operacion[0], operacion[1], operacion[2])
        
        println "${operacion[0]} ${operacion[2]} ${operacion[1]} = $resultado"      
    }
    
    static def calcula(String op1, String op2, String operador) {
        switch (operador) {
            case "+":
                return op1.toInteger() + op2.toInteger()
                
            case "-":
                return op1.toInteger() - op2.toInteger()
                
            case "*":
                return op1.toInteger() * op2.toInteger()
            
            case "/":
                return op1.toInteger() / op2.toInteger()
        
            default:
                return 0
        }
    }
}